package cn.cinz.config;

import cn.cinz.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.WebContentInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * webMvc 配置类
 *
 * @Author: zengcheng
 * @Description: 解决跨域问题，拦截请求资源
 * @Date: 2021/9/9 23:35
 */
@Configuration
public class AdminWebConfig implements WebMvcConfigurer {

    @Bean
    public LoginInterceptor getLoginInterceptor(){
        return new LoginInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        /**
         * 解决跨域问题
         */
        registry.addInterceptor(new HandlerInterceptor(){
            @Override
            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
                response.addHeader("Access-Control-Allow-Origin", "*");
                response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
                response.addHeader("Content-Type", "application/x-www-form-urlencoded");
                response.addHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,token");
                return true;
            }
        });

        /**
         * //拦截所有请求
         * //放行资源
         */
        registry.addInterceptor(getLoginInterceptor())//getLoginInterceptor() @Bean 让spring托管
                .addPathPatterns("/**")
                .excludePathPatterns("/","/css/**","/fonts/*","/images/*","/js/*","/public/*","/login","/loginVerification","/favicon.ico","/error")
        ;


    }
}