package cn.cinz.interceptor;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 拦截器
 *
 * @Author: zengcheng
 * @Description: 拦截器：放行请求
 * @Date: 2021/9/9 23:11
 */
@Configuration
public class LoginInterceptor implements HandlerInterceptor {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);


    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


        String url = request.getRequestURI();

        //logger.info("拦截器--->(目前请求路径):"+url);

//       if (url.indexOf("/")>0 || url.indexOf("/login")>0 || url.indexOf("qqLogin")>0 || url.indexOf("/static/**")>0 || url.indexOf("loginVerification")>0){
//           return true;
//       }
//
//
//        //获取Headers中的token参数
//        String token = request.getHeader("token");
//
//        token = token == null ? "" : token;
//
//        //查询Redis中是否存在  查询key在Redis中的剩余时间
//        Long expire = redisTemplate.getExpire(token);
//        logger.info(token);
//
//        if (expire > 0){
//
//            //是登录状态
//            //重置key剩余时间  key,时间,分钟
//            redisTemplate.expire(token,30L, TimeUnit.MINUTES);
//
//            return true;
//        }else {
//            //未登录用户 返回到登陆界面
//            logger.info("请先登陆");
//            String info = JSONObject.toJSONString(ResultJson.error().message("未登录").code(103));
//
//            response.setContentType("json/text;charset=utf-8");
//            PrintWriter out = response.getWriter();
//            out.write(info);
//
//            return false;

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}