package cn.cinz.controller.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 后台首页控制器
 *
 * @Description: 后台首页控制器
 * @Author zengcheng
 * @Date 2021/11/6 22:42
 * @Version 1.0
 */
@Controller
public class MenusHomeController {

    /**
     * 后台首页跳转
     * @param model
     * @return
     */
    @RequestMapping("/index")
    public String test(Model model){

        model.addAttribute("MenusTitle","后台首页");

        return "index";
    }

}
