package cn.cinz.controller.login;

import cn.cinz.common.ResultJson;
import cn.cinz.common.model.forum.user.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 登录控制器
 *
 * @Description: 登录控制器
 * @Author zengcheng
 * @Date 2021/11/7 15:21
 * @Version 1.0
 */
@Controller
public class LoginController {

    @RequestMapping({"","/","/login"})
    public String index(){
        return "login";
    }

    /**
     * 登录验证
     * @param user
     * @return
     */
    @PostMapping("/loginValidation")
    @ResponseBody
    public ResultJson loginValidation(@RequestBody User user){
        return ResultJson.success();
    }

}
