package cn.cinz.controller.forum.version;

import cn.cinz.common.ResultJson;
import cn.cinz.common.model.forum.version.ForumVersion;
import cn.cinz.service.forum.version.ForumVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 描述
 *
 * @Description: 论坛版本控制器
 * @Author zengcheng
 * @Date 2021/11/14 20:25
 * @Version 1.0
 */
@Controller
@RequestMapping("/forum/version")
public class MenusVersionController {

    @Autowired
    private ForumVersionService forumVersionService;

    /**
     * 版本管理首页跳转
     * @return
     */
    @RequestMapping({"","/","/list"})
    public String index(Model model){

        model.addAttribute("MenusTitle","版本管理");

        return "forum/version/list";
    }

    @RequestMapping("/listData")
    @ResponseBody
    public ResultJson versionListData(ForumVersion record){
        try {
            List<ForumVersion> forumVersionList = forumVersionService.selectForumVersionList(record);
            int count = forumVersionService.countForumVersion(record);
            return ResultJson.success().data(forumVersionList).total(count);
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }
    }

    /**
    * 新增一个版本
    * @Description: 新增一个版本
    * @Author zengcheng
    * @Date 2021/11/21 16:33
    * @Version 1.0
    */
    @RequestMapping("/add")
    @ResponseBody
    public ResultJson add(@RequestBody ForumVersion forumVersion){
        try{
            forumVersionService.insert(forumVersion);
            return ResultJson.success();
        }catch (Exception e){
            return ResultJson.error().message(e.getMessage());
        }

    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResultJson delete(@RequestBody ForumVersion forumVersion){
        try {
            forumVersionService.deleteByPrimaryKey(forumVersion.getId());
            return ResultJson.success();
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }
    }

}
