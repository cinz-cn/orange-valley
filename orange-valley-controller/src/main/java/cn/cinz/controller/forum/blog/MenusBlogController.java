package cn.cinz.controller.forum.blog;

import cn.cinz.common.ResultJson;
import cn.cinz.common.model.forum.blog.Blog;
import cn.cinz.common.model.forum.blog.BlogAdd;
import cn.cinz.common.model.forum.blog.BlogQuery;
import cn.cinz.service.forum.blog.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 博客管理
 *
 * @Description: 博客管理
 * @Author zengcheng
 * @Date 2021/11/7 23:40
 * @Version 1.0
 */
@Controller
@RequestMapping("/forum/blog")
public class MenusBlogController {

    @Autowired
    private BlogService blogService;

    /**
     * 博客管理页面跳转
     * @param model
     * @return
     */
    @RequestMapping({"","/","/list"})
    public String index(Model model){

        model.addAttribute("MenusTitle","博客管理");

        return "forum/blog/list";
    }


    @RequestMapping("/listData")
    @ResponseBody
    public ResultJson blogListData(@RequestBody BlogQuery blogQuery){

        List<BlogAdd> list = null;
        try {
            list = blogService.selectBlogList(blogQuery);
            if (list != null && list.size() > 0){
                Integer count = blogService.countBlog();
                return ResultJson.success().data(list).total(count);
            }else {
                return ResultJson.error().message("未找到");
            }

        } catch (Exception e) {
            if (e instanceof Exception){
                return ResultJson.error().message("未找到");
            }else{
                return ResultJson.error().message(e.getMessage());
            }
        }
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResultJson blogDelete(@RequestBody Blog blog){
        try {
            blogService.delete(blog);
            return ResultJson.success();
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }
    }
}
