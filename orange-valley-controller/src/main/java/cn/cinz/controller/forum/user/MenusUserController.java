package cn.cinz.controller.forum.user;

import cn.cinz.common.ResultJson;
import cn.cinz.common.model.forum.user.User;
import cn.cinz.common.model.forum.user.UserQuery;
import cn.cinz.service.forum.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 用户菜单控制器
 *
 * @Description: 用户菜单控制器
 * @Author zengcheng
 * @Date 2021/11/7 15:37
 * @Version 1.0
 */
@Controller
@RequestMapping("/forum/user")
public class MenusUserController {

    @Autowired
    private UserService userService;

    /**
     * 用户管理首页跳转
     * @return
     */
    @GetMapping({"","/","/list"})
    public String index(Model model){

        model.addAttribute("MenusTitle","用户管理");

        return "forum/user/list";
    }

    @RequestMapping("/listData")
    @ResponseBody
    public ResultJson userListData(@RequestBody UserQuery userQuery){
        try {
            List<User> userList = userService.selectUserList(userQuery);
            Integer count = userService.countUser();
            return ResultJson.success().data(userList).total(count);
        }catch (Exception e){
            return ResultJson.error().message(e.getMessage());
        }
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResultJson userDelete(@RequestBody User user){
        try {
            userService.delete(user);
            return ResultJson.success();
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }
    }

}
