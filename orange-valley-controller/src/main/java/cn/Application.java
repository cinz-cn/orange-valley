package cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author zengcheng
 */
@SpringBootApplication
(scanBasePackages =
    {
        "cn.cinz.**",
        "cn.cinz.service.forum.**.*","cn.cinz.common.model.forum.*" //扫描forum service-model
    }
)
@MapperScan
(
    {
        "cn.cinz.mapper.forum.*", //扫描forum mapper
    }
)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
