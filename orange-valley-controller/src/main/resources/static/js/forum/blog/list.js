var vdata = null;
$(function() {
    vdata = new Vue({
        el: '#index',
        data: {
            //查询
            query: {
                page: 1,
                limit: "10"
            },
            //服务器传过来的数据
            data:{
                total: "",
                data: {
                },
                page: "",
            },
            //查询状态
            search: true,
        },
        methods: {
            //查找数据
            searchData() {
                $.ajax({
                    url: '/forum/blog/listData',
                    data: JSON.stringify(vdata.query),
                    method:"post",
                    contentType:"application/json;charset=utf-8",
                    success: function (data) {
                        if (data.success) {
                            vdata.data = data
                        } else {
                            alert(vdata.message)//获取失败消息
                        }

                    }
                })
            },
            deleted:function (id,title){
                let _this = this;
                if (confirm("您确定删除【 "+title+" 】吗")) {
                    $.ajax({
                        url: '/forum/blog/delete',
                        data: JSON.stringify({"id": id}),
                        method: "post",
                        contentType: "application/json;charset=utf-8",
                        success: function (data) {
                            if (data.success) {
                                _this.$notify({
                                    title: '成功',
                                    message: '删除成功',
                                    type: 'success',
                                    duration: 3000,
                                });
                                vdata.searchData()
                            } else {
                                alert(vdata.message)//获取失败消息
                            }

                        }
                    })
                }
            },
            page:function (pageNumber){
                if (pageNumber != null && pageNumber != ''){
                    vdata.query.page = pageNumber;
                }
                //截取页 当前页 ~ 后四页
                //vdata.page = vdata.data.page.slice(--page,page+5)
                vdata.searchData()
            }
        },
    })
    vdata.searchData();
})