
var vdata = null;
$(function() {
    vdata =new Vue({
        el:'#app',
        data() {
            return {
                forum:{
                    version: '',
                    description: '',
                    versionIcon: '',
                    nodeColor: '',
                },
                rules:{
                    version:[
                        {required:true,message:'请输入版本号',trigger:'blur',}
                    ],
                    description:[
                        {required:true,message:'请输入版本内容',trigger:'blur',}
                    ],
                    versionIcon:[
                        {required:true,message:'请选择版本图标',trigger:'blur',}
                    ],
                    nodeColor:[
                        {required:true,message:'请选择节点颜色',trigger:'blur',}
                    ],
                },
                color:[
                    {
                        value: 'primary',
                        label: '蓝色'
                    }, {
                        value: 'success',
                        label: '绿色'
                    }, {
                        value: 'text',
                        label: '灰色'
                    }, {
                        value: 'warning',
                        label: '橙色'
                    }, {
                        value: 'danger',
                        label: '红色'
                    }
                ],
                options:[
                    {
                        value: 'el-icon-delete',
                        label: '删除'
                    }, {
                        value: 'el-icon-star-off',
                        label: '收藏'
                    }, {
                        value: 'el-icon-circle-close',
                        label: '错误'
                    }, {
                        value: 'el-icon-warning-outline',
                        label: '警告'
                    }, {
                        value: 'el-icon-edit-outline',
                        label: '修改'
                    }
                ],
                data:{
                    total:'',
                    data:{

                    },
                },
                activities: [{
                    content: '支持使用图标',
                    timestamp: '2018-04-12 20:46',
                    size: 'large',
                    type: 'primary',
                    icon: 'el-icon-more'
                }, {
                    content: '支持自定义颜色',
                    timestamp: '2018-04-03 20:46',
                    color: '#0bbd87'
                }, {
                    content: '支持自定义尺寸',
                    timestamp: '2018-04-03 20:46',
                    size: 'large'
                }, {
                    content: '默认样式的节点',
                    timestamp: '2018-04-03 20:46'
                }],
                dialogVisible: false,
            };
        },
        methods:{
            searchData(){
                $.ajax({
                    url: '/forum/version/listData',
                    //data: JSON.stringify(this.forum),
                    method:"post",
                    contentType:"application/json;charset=utf-8",
                    success: function (data) {
                        if (data.success) {
                            vdata.data = data;
                        } else {
                            alert(vdata.message)//获取失败消息
                        }

                    }
                })
            },
            add(formName){
                let _this = this;
                this.$refs[formName].validate((valid) => {
                    if (valid) {
                        this.dialogVisible = false
                        console.log(this.forum)
                        $.ajax({
                            url: '/forum/version/add',
                            data: JSON.stringify(this.forum),
                            method:"post",
                            contentType:"application/json;charset=utf-8",
                            success: function (data) {
                                if (data.success) {
                                    _this.$notify({
                                        title: '成功',
                                        message: '添加成功',
                                        type: 'success',
                                        duration: 3000,
                                    });
                                    vdata.searchData();
                                } else {
                                    alert(vdata.message)//获取失败消息
                                }

                            }
                        })

                    } else {
                        //提交失败
                        return false;
                    }
                });
            },
            edit(){
                bootbox.load('/control/fixedAsset/asset/edit', {
                    size : 'large',
                    closeButton: true,
                });
            },
            deleted(id){
                this.$confirm('此操作将永久删除该版本, 是否继续?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {//点击确定处理
                    $.ajax({
                        url: '/forum/version/delete',
                        data: JSON.stringify({"id":id}),
                        method:"post",
                        contentType:"application/json;charset=utf-8",
                        success: function (data) {
                            if (data.success) {
                                vdata.searchData()
                            } else {
                                alert(vdata.message)//获取失败消息
                            }
                        }
                    })
                    this.$message({
                        type: 'success',
                        message: '删除成功!'
                    });
                }).catch(() => {//点击取消处理
                    this.$message({
                        type: 'info',
                        message: '已取消删除'
                    });
                });
            },
        },
    });
    vdata.searchData();
})

