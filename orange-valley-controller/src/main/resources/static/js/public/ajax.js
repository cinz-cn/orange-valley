/**
 * 通用Ajax方法
 * @param url 请求数据地址
 * @param data 发送的数据
 * @param method 请求方式
 * @constructor
 */
function Ajax(url, query, method, data){
    $.ajax({
        url:url,
        data:JSON.stringify(query),
        contentType:"json",
        method:method,
        success:function (data){
            if (data.success){
                Ajax.data = data
            }else{
                alert(data.message)
            }
        }
    })
}