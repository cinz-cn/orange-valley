package cn.cinz.service.forum.user.impl;

import cn.cinz.common.model.forum.user.User;
import cn.cinz.common.model.forum.user.UserQuery;
import cn.cinz.mapper.forum.user.UserMapper;
import cn.cinz.service.forum.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述
 *
 * @Description: TOO描述
 * @Author zengcheng
 * @Date 2021/11/13 17:46
 * @Version 1.0
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> selectUserList(UserQuery userQuery) {
        userQuery.setPage(userQuery.getPage()*userQuery.getLimit()-userQuery.getLimit());
        userQuery.setLimit(userQuery.getLimit());

        return userMapper.selectUserList(userQuery);
    }

    @Override
    public Integer countUser() {
        return userMapper.countUser();
    }

    @Override
    public int delete(User user) throws Exception {
        return userMapper.delete(user);
    }
}
