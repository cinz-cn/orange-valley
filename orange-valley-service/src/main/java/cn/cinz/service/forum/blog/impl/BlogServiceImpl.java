package cn.cinz.service.forum.blog.impl;

import cn.cinz.common.model.forum.blog.Blog;
import cn.cinz.common.model.forum.blog.BlogAdd;
import cn.cinz.common.model.forum.blog.BlogQuery;
import cn.cinz.mapper.forum.blog.BlogMapper;
import cn.cinz.service.forum.blog.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述
 *
 * @Description: TOO描述
 * @Author zengcheng
 * @Date 2021/11/13 14:57
 * @Version 1.0
 */
@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogMapper blogMapper;

    @Override
    public List<BlogAdd> selectBlogList(BlogQuery blogQuery) {
        blogQuery.setPage((blogQuery.getPage() * blogQuery.getLimit()) - blogQuery.getLimit());
        blogQuery.setLimit(blogQuery.getLimit());
        return blogMapper.selectBlogList(blogQuery);
    }

    @Override
    public Integer countBlog() {
        return blogMapper.countBlog();
    }

    @Override
    public int delete(Blog blog) {
        return blogMapper.delete(blog);
    }

    @Override
    public int insertBlog(Blog blog) {
        return blogMapper.insert(blog);
    }
}
