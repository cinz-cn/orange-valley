package cn.cinz.service.forum.blog;

import cn.cinz.common.model.forum.blog.Blog;
import cn.cinz.common.model.forum.blog.BlogAdd;
import cn.cinz.common.model.forum.blog.BlogQuery;

import java.util.List;

/**
*
* @Description: TOO
* @Author zengcheng
* @Date 2021/11/13 14:59
* @Version 1.0
*/
public interface BlogService {

    /**
     * 查询博客列表
     * @param blogQuery
     * @return
     */
    List<BlogAdd> selectBlogList(BlogQuery blogQuery) throws Exception;

    /**
     * 统计博客条数 用于分页
     * @return
     */
    Integer countBlog() throws Exception;

    /**
     * 删除一篇博客
     * @param blog
     * @return
     */
    int delete(Blog blog) throws Exception;

    /**
     * 插入博客
     * @param blog
     * @return
     */
    int insertBlog(Blog blog);

}
