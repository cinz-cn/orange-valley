package cn.cinz.service.forum.version;

import cn.cinz.common.model.forum.version.ForumVersion;

import java.util.List;

/**
 * 论坛版本
 *
 * @Description: 论坛版本
 * @Author zengcheng
 * @Date 2021/11/21 16:26
 * @Version 1.0
 */
public interface ForumVersionService {

    int deleteByPrimaryKey(String id) throws Exception;

    int insert(ForumVersion record);

    int insertSelective(ForumVersion record);

    ForumVersion selectByPrimaryKey(String id) throws Exception;

    List<ForumVersion> selectForumVersionList(ForumVersion record);

    int updateByPrimaryKeySelective(ForumVersion record);

    int updateByPrimaryKey(ForumVersion record);

    int countForumVersion(ForumVersion forumVersion);

}
