package cn.cinz.service.forum.user;

import cn.cinz.common.model.forum.user.User;
import cn.cinz.common.model.forum.user.UserQuery;

import java.util.List;

/**
 * 描述
 *
 * @Description: TOO描述
 * @Author zengcheng
 * @Date 2021/11/13 14:56
 * @Version 1.0
 */
public interface UserService {

    /**
     * 获得用户列表
     * @param userQuery
     * @return
     */
    List<User> selectUserList(UserQuery userQuery);

    /**
     * 统计用户数量
     * @return
     */
    Integer countUser();

    /**
     * 删除一篇博客
     * @param user
     * @return
     */
    int delete(User user) throws Exception;
}
