package cn.cinz.service.forum.version.impl;

import cn.cinz.common.model.forum.version.ForumVersion;
import cn.cinz.common.util.UUIDUtils;
import cn.cinz.mapper.forum.version.ForumVersionMapper;
import cn.cinz.service.forum.version.ForumVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 描述
 *
 * @Description: TOO描述
 * @Author zengcheng
 * @Date 2021/11/21 16:27
 * @Version 1.0
 */
@Service
public class ForumVersionServiceImpl implements ForumVersionService {

    @Autowired
    private ForumVersionMapper forumVersionMapper;

    @Override
    public List<ForumVersion> selectForumVersionList(ForumVersion record) {
        return forumVersionMapper.selectForumVersionList(record);
    }

    @Override
    public int updateByPrimaryKeySelective(ForumVersion record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(ForumVersion record) {
        return 0;
    }

    @Override
    public int deleteByPrimaryKey(String id) throws Exception {
        return forumVersionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ForumVersion record) {
        record.setId(UUIDUtils.getId());
        record.setCreateDate(new Date());
        record.setDeleted(0);
        return forumVersionMapper.insert(record);
    }

    @Override
    public int insertSelective(ForumVersion record) {
        return 0;
    }

    @Override
    public ForumVersion selectByPrimaryKey(String id) throws Exception {
        return null;
    }

    @Override
    public int countForumVersion(ForumVersion forumVersion) {
        return forumVersionMapper.countForumVersion(forumVersion);
    }

}
