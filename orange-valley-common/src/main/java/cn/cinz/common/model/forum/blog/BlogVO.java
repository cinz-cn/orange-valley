package cn.cinz.common.model.forum.blog;

import cn.cinz.common.model.forum.user.User;

/**
 *
 * @Description: 博客数据返回实体
 * @Author zengcheng
 * @Date 2021/10/10 17:59
 * @Version 1.0
 */
public class BlogVO {

    private BlogQuery blogQuery;
    private User user;

    public BlogQuery getBlogQuery() {
        return blogQuery;
    }

    public void setBlogQuery(BlogQuery blogQuery) {
        this.blogQuery = blogQuery;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "BlogVO{" +
                "blogQuery=" + blogQuery +
                ", user=" + user +
                '}';
    }
}
