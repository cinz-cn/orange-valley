package cn.cinz.common.model.forum.version;

import java.io.Serializable;
import java.util.Date;

public class ForumVersion implements Serializable {
    private String id;

    private String version;

    private String description;

    private String nodeColor;

    private Date createDate;

    private String createUser;

    private String versionIcon;

    private Integer deleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getNodeColor() {
        return nodeColor;
    }

    public void setNodeColor(String nodeColor) {
        this.nodeColor = nodeColor == null ? null : nodeColor.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public String getVersionIcon() {
        return versionIcon;
    }

    public void setVersionIcon(String versionIcon) {
        this.versionIcon = versionIcon == null ? null : versionIcon.trim();
    }

    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 设置 是否删除
     * @param deleted
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "ForumVersion{" +
                "id='" + id + '\'' +
                ", version='" + version + '\'' +
                ", description='" + description + '\'' +
                ", nodeColor='" + nodeColor + '\'' +
                ", createDate=" + createDate +
                ", createUser='" + createUser + '\'' +
                ", versionIcon='" + versionIcon + '\'' +
                ", deleted='" + deleted + '\'' +
                '}';
    }
}