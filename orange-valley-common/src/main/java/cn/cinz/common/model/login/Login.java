package cn.cinz.common.model.login;

import java.io.Serializable;

/**
 * 系统用户登录表(Login)实体类
 *
 * @author makejava
 * @since 2022-01-19 18:13:18
 */
public class Login implements Serializable {
    private static final long serialVersionUID = 481880898779292991L;

    /**
     * id
     */
    private String id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 状态
     */
    private String status;

    /**
     * 是否被删除，0=正常，1=已删除。
     */
    private Integer deleted;


    /**
     * @description: 获取 id
     *
     * @return id
     */
    public String getId() {
        return this.id;
    }

    /**
     * @description:设置 id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @description: 获取 用户名
     *
     * @return 用户名
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @description:设置 用户名
     *
     * @param username 用户名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @description: 获取 密码
     *
     * @return 密码
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @description:设置 密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @description: 获取 状态
     *
     * @return 状态
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @description:设置 状态
     *
     * @param status 状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @description: 获取 是否被删除，0=正常，1=已删除。
     *
     * @return 是否被删除，0=正常，1=已删除。
     */
    public Integer getDeleted() {
        return this.deleted;
    }

    /**
     * @description:设置 是否被删除，0=正常，1=已删除。
     *
     * @param deleted 是否被删除，0=正常，1=已删除。
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}

