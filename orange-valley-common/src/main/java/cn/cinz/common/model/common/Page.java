package cn.cinz.common.model.common;

/**
 * 描述
 *
 * @Description: 分页
 * @Author zengcheng
 * @Date 2021/11/7 18:06
 * @Version 1.0
 */
public class Page {

    private Integer page;
    private Integer limit;
    private Object object;

    public Page() {
        super();
    }

    public Page(Integer page, Integer limit, Object object) {
        this.page = page;
        this.limit = limit;
        this.object = object;
    }

    public Integer getPage() {
        return page;
    }

    /**
     * 页码
     * @param page
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    /**
     * 当前页条数
     * @param limit
     */
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Object getObject() {
        return object;
    }

    /**
     * 查询的实体
     * @param object
     */
    public void setObject(Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "Page{" +
                "page=" + page +
                ", limit=" + limit +
                ", object=" + object +
                '}';
    }
}
