package cn.cinz.common;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 描述
 *
 * @Description: TOO描述
 * @Author zengcheng
 * @Date 2021/11/21 9:28
 * @Version 1.0
 */
public class ResultData implements Serializable {

    @ApiModelProperty("页数据")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")//避免从数据库取日期的时候 变成时间戳
    List<Object> data;

    @ApiModelProperty("页码")
    Integer pageNumber;

    @ApiModelProperty("页头")
    Integer pageTop;

    @ApiModelProperty("页尾")
    Integer pageBottom;

    @ApiModelProperty("总页数")
    Integer pageTotal;

    @ApiModelProperty("数据总数")
    Integer total;

    @ApiModelProperty("页内数据条数(例如1-10条数据)")
    Integer limit;

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageTop() {
        return pageTop;
    }

    public void setPageTop(Integer pageTop) {
        this.pageTop = pageTop;
    }

    public Integer getPageBottom() {
        return pageBottom;
    }

    public void setPageBottom(Integer pageBottom) {
        this.pageBottom = pageBottom;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Integer pageTotal) {
        this.pageTotal = pageTotal;
    }
}
