package cn.cinz.common;

/**
 * 公共JSON 状态
 *
 * @Author: zengcheng
 * @Description: 公共JSON 状态
 * @Date: 2021/9/4 15:21
 */
public interface ResultCode {

    public static Integer SUCCESS = 200; //成功

    public static Integer ERROR = 500; //失败

}
