package cn.cinz.common.enums;

/**
* 短信状态枚举
* @ClassName ESendSms
* @Description 短信状态枚举
* @Author zengcheng
* @Date 2021/10/2 22:04
* @Version 1.0
*/
public enum ESendSms {

    发送成功(800,"发送成功"),
    发送失败(801,"发送失败"),
    一分钟只可发送一条(802,"一分钟只可发送一条"),
    一小时只可发送三条(803,"一小时只可发送三条"),
    当日发送次数已达上限(804,"当日发送次数已达上限"),
    验证码正确(810,"验证码正确"),
    验证码错误(811,"验证码错误或已失效"),
    验证码不能为空(899,"验证码不能为空"),
    ;

    private Integer value;
    private String name;

    ESendSms(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
