package cn.cinz.common.enums;
/**
* 登陆状态枚举
* @ClassName ELoginStatus
* @Description 功能描述
* @Author zengcheng
* @Date 2021/10/3 0:20
* @Version 1.0
*/
public enum ELoginStatus {

    登陆成功(0,"登陆成功"),
    登陆失败(1,"登陆失败"),

    ;


    private Integer value;
    private String name;

    ELoginStatus(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
