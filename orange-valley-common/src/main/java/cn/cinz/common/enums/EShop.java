package cn.cinz.common.enums;

/**
* 支付/商品枚举
* @ClassName EShop
* @Description 功能描述
* @Author zengcheng
* @Date 2021/10/5 18:25
* @Version 1.0
*/
public enum EShop {

    支付成功(1,"success"),
    支付失败(2,"error")
    ;


    private Integer name;
    private String value;

    EShop(){}

    EShop(Integer name, String value) {
        this.name = name;
        this.value = value;
    }

    public Integer getName() {
        return name;
    }

    public void setName(Integer name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
