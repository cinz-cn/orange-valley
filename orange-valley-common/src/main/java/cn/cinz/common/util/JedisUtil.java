package cn.cinz.common.util;

import redis.clients.jedis.Jedis;

/**
* redis工具类 连接redis，销毁redis
* @Author: zengcheng
* @Description: 连接redis，销毁redis
* @Date: 2021/8/23 23:26
*
*/
public class JedisUtil {

    private static Jedis jedis = null;
    private static final String HOST = "127.0.0.1";//redis 连接地址

    /**
     * 单例模式 创建 jedis对象，并获得连接
     * 默认连接 127.0.0.1
     * @return jedis
     */
    public static Jedis getRedis(){
        jedis = new Jedis(HOST,6379);
        return jedis;
    }

    /**
     * 可变地址，实现需要连接redis的地址
     * 输入连接地址
     * @param host 可变连接地址
     * @return jedis
     */
    public static Jedis getRedis(String host){
        jedis = new Jedis(host,6379);
        return jedis;
    }

    /**
     * redis回收 传入之前创建连接的jedis对象,即可完成回收
     * @param jedis 传入之前创建连接的jedis对象
     */
    public static void releaseRedis(Jedis jedis){

        try {

        } finally {
            if (jedis!=null){
                jedis.close();
            }
        }

    }


}
