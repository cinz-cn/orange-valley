package cn.cinz.common.util;

import java.util.UUID;

/**
* UUID生成类
* @Author: zengcheng
* @Description: UUID生成
* @Date: 2021/9/20 0:19
*
*/
public class UUIDUtils {

    /**
     * 生成一个随机id
     * @return
     */
    public static String getId() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }
}
