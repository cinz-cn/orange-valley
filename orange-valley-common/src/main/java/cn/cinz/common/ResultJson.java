package cn.cinz.common;

import cn.cinz.common.ResultCode;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公共JSON格式类
 *
 * @Author: zengcheng
 * @Description: 返回公共JSON信息
 * @Date: 2021/9/4 15:23
 */
public class ResultJson {

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    /**
     * 响应编码
     * 100-请求成功
     * 101-请求异常
     * 103-未登录
     * 104-请求失败
     */
    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;


    @ApiModelProperty(value = "页码")
    private Integer page;

    @ApiModelProperty(value = "数据总条数")
    private Integer total;

    @ApiModelProperty(value = "返回数据")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")//避免从数据库取日期的时候 变成时间戳
    private Object data;

    private ResultJson() {
    }

    /**
     * 失败时调用这个方法
     * @return
     */
    public static ResultJson error() {
        ResultJson resultJson = new ResultJson();
        resultJson.setSuccess(false);
        resultJson.setCode(ResultCode.ERROR);
        resultJson.setMessage("失败");
        return resultJson;
    }

    /**
     * 成功时调用这个方法
     * @return
     */
    public static ResultJson success() {
        ResultJson resultJson = new ResultJson();
        resultJson.setSuccess(true);
        resultJson.setCode(ResultCode.SUCCESS);
        resultJson.setMessage("成功");
        return resultJson;
    }

    /**
     * 消息
     * @param message
     * @return
     */
    public ResultJson message(String message) {
        this.setMessage(message);
        return this;
    }

    /**
     * 状态码
     * @param code
     * @return
     */
    public ResultJson code(Integer code) {
        this.setCode(code);
        return this;
    }


    /**
     * 数据总条数
     * @param total
     * @return
     */
    public ResultJson total(Integer total) {
        this.total = total;
        this.page = (this.total / 10 + 1);
        return this;
    }

    /**
     * 数据
     * @param value
     * @return
     */
    public ResultJson data(Object value) {
        this.data = value;
        return this;
    }



    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
