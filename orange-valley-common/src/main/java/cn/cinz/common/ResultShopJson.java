package cn.cinz.common;


import cn.cinz.common.enums.EShop;
import cn.cinz.common.model.shop.PayModel;
import io.swagger.annotations.ApiModelProperty;

/**
* 支付成功JSON格式类
* @ClassName ResultShopJson
* @Description 支付成功JSON格式类
* @Author zengcheng
* @Date 2021/10/5 16:38
* @Version 1.0
*/
public class ResultShopJson {

    private String order_no;//商户订单号
    private String subject;//商品名称
    private Integer pay_type;//支付类型 支付宝=43 微信支付=44
    private float money;//订单金额
    private float realmoney;//实际支付金额
    private String result;//支付结果 支付成功=success，其它均为失败
    private String xddpay_order;//小叮当订单号
    private Integer app_id;//接口APP_ID
    private String extra;//商户自定义数据
    private String sign;//秘钥

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;


    /**
     * 成功时调用这个方法
     * @return
     */
    public static ResultShopJson ok(PayModel payModel) {
        ResultShopJson r = new ResultShopJson();

        r.setOrder_no(payModel.getOrder_no());
        r.setSubject(payModel.getSubject());
        r.setPay_type(payModel.getPay_type());
        r.setMoney(payModel.getMoney());
        r.setRealmoney(payModel.getRealmoney());
        r.setResult(EShop.支付成功.getValue());
        r.setXddpay_order(payModel.getXddpay_order());
        r.setApp_id(payModel.getApp_id());
        r.setExtra(payModel.getExtra());
        r.setSign(payModel.getSign().toUpperCase());


        r.setSuccess(true);
        r.setCode(ResultCode.SUCCESS);
        r.setMessage("成功");
        return r;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getPay_type() {
        return pay_type;
    }

    public void setPay_type(int pay_type) {
        this.pay_type = pay_type;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public float getRealmoney() {
        return realmoney;
    }

    public void setRealmoney(float realmoney) {
        this.realmoney = realmoney;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getXddpay_order() {
        return xddpay_order;
    }

    public void setXddpay_order(String xddpay_order) {
        this.xddpay_order = xddpay_order;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
