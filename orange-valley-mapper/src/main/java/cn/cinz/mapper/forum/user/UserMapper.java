package cn.cinz.mapper.forum.user;

import cn.cinz.common.model.forum.user.User;
import cn.cinz.common.model.forum.user.UserQuery;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 描述
 *
 * @Description: TOO描述
 * @Author zengcheng
 * @Date 2021/11/7 17:56
 * @Version 1.0
 */
@Repository
public interface UserMapper extends Mapper<User> {

    /**
     * 获得用户列表
     * @param userQuery
     * @return
     */
    List<User> selectUserList(UserQuery userQuery);

    /**
     * 统计用户数量
     * @return
     */
    Integer countUser();

}
