package cn.cinz.mapper.forum.blog;

import cn.cinz.common.model.forum.blog.Blog;
import cn.cinz.common.model.forum.blog.BlogAdd;
import cn.cinz.common.model.forum.blog.BlogQuery;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
*
* @Description: TOO
* @Author zengcheng
* @Date 2021/11/7 23:44
* @Version 1.0
*/
@Repository
public interface BlogMapper extends Mapper<Blog> {

    /**
     * 查询博客列表
     * @param blogQuery
     * @return
     */
    List<BlogAdd> selectBlogList(BlogQuery blogQuery);

    /**
     * 统计博客条数 用于分页
     * @return
     */
    Integer countBlog();


}
