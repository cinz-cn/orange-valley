package cn.cinz.mapper.forum.version;

import cn.cinz.common.model.forum.version.ForumVersion;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface ForumVersionMapper{

    int deleteByPrimaryKey(String id) throws Exception;

    int insert(ForumVersion record);

    int insertSelective(ForumVersion record);

    ForumVersion selectByPrimaryKey(String id) throws Exception;

    List<ForumVersion> selectForumVersionList(ForumVersion record);

    int updateByPrimaryKeySelective(ForumVersion record);

    int updateByPrimaryKey(ForumVersion record);

    int countForumVersion(ForumVersion forumVersion);

}